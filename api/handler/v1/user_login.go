package v1

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	"gitlab.com/simple_chat/api/handler/v1/http"
	"gitlab.com/simple_chat/api/models"
	"gitlab.com/simple_chat/pkg/etc"
	"gitlab.com/simple_chat/pkg/mail"
	"time"
)

func (h *handlerV1) Login(c *gin.Context) {
	email := c.Query("email")
	isMail, err := h.Storage.ChatApp().CheckField(&models.CheckFieldReq{
		Field: "email",
		Value: email,
	})
	if err != nil {
		h.handleResponse(c, http.BadRequest, err)
		return
	}
	if !isMail.Exists {
		h.handleResponse(c, http.BadRequest, models.Message{Message: "Email not found. Please try again."})
		return
	}
	code := etc.GenerateCode(4)
	err = mail.MailSender(email, code)
	if err != nil {
		h.handleResponse(c, http.InternalServerError, err)
		return
	}
	cacheCode := models.RedisSave{
		Code:        code,
		SendDate:    time.Now().Format(time.RFC822),
		ExpiredDate: time.Now().Add(time.Minute * 2).Format(time.RFC822),
	}

	inSavedRedis, err := json.Marshal(cacheCode)
	if err != nil {
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}
	err = h.Redis.SetWithTTL(email, string(inSavedRedis), 600)
	if err != nil {
		h.handleResponse(c, http.InternalServerError, err)
		return
	}

	h.handleResponse(c, http.OK, models.Message{
		Message: "Code sent successfully to email!!!",
	})

}

func (h *handlerV1) Verify(c *gin.Context) {
	email := c.Query("email")
	code := c.Param("code")
	_, err := h.Redis.Exists(email)
	if err != nil {
		h.handleResponse(c, http.InternalServerError, models.ResponseError{
			Error: "Your email has been deleted, Please resend",
		})
		return
	}
	res, err := h.Redis.Get(email)
	if err != nil {
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}
	if res == nil {
		h.handleResponse(c, http.NotFound, err.Error())
		return
	}
	resStr := cast.ToString(res)
	var body models.RedisSave
	err = json.Unmarshal([]byte(resStr), &body)
	if err != nil {
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}

	if body.Code != code {
		h.handleResponse(c, http.Conflict, models.ResponseError{
			Error: "Wrong code",
		})
		return
	}
	result, err := h.Storage.ChatApp().GetUserByEmail(email)
	if err != nil {
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}
	h.JwtHandler.Sub = result.Id
	h.JwtHandler.Aud = []string{"chat-app"}
	h.JwtHandler.SigninKey = h.Cfg.SigningKey
	h.JwtHandler.Log = h.Log
	h.JwtHandler.Role = "user"
	tokens, err := h.JwtHandler.GenerateAuthJWT()
	if err != nil {
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}
	result.AccessToken = tokens[0]
	h.handleResponse(c, http.OK, result)
}
