package v1

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/simple_chat/api/handler/v1/http"
	"gitlab.com/simple_chat/api/models"
	"strconv"
)

func (h *handlerV1) RegisterUser(c *gin.Context) {
	var body models.UserRequest
	err := c.ShouldBindJSON(&body)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err)
		return
	}
	h.JwtHandler.Aud = []string{"chat-app"}
	h.JwtHandler.SigninKey = h.Cfg.SigningKey
	h.JwtHandler.Log = h.Log
	h.JwtHandler.Role = "user"
	tokens, err := h.JwtHandler.GenerateAuthJWT()
	if err != nil {
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}
	body.RefreshToken = tokens[1]
	res, err := h.Storage.ChatApp().CreateUser(&body)
	if err != nil {
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}
	result := models.UserLoginResponse{
		Id:          res.Id,
		FirstName:   res.FirstName,
		LastName:    res.LastName,
		UserName:    res.UserName,
		Bio:         res.Bio,
		Email:       res.Email,
		Image:       res.Image,
		Phone:       res.Phone,
		AccessToken: tokens[0],
	}
	h.handleResponse(c, http.Created, result)
}

func (h *handlerV1) GetUser(c *gin.Context) {
	id := c.Param("id")
	userID, err := strconv.Atoi(id)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}
	res, err := h.Storage.ChatApp().GetUser(userID)
	if err != nil {
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}
	h.handleResponse(c, http.OK, res)
}

func (h *handlerV1) GetUserList(c *gin.Context) {
	res, err := h.Storage.ChatApp().GetUserList()
	if err != nil {
		h.handleResponse(c, http.InternalServerError, err)
		return
	}
	h.handleResponse(c, http.OK, res)
}

func (h *handlerV1) GetSearchedUsers(c *gin.Context) {
	key := c.Query("keyword")
	res, err := h.Storage.ChatApp().SearchUsers(key)
	if err != nil {
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}
	h.handleResponse(c, http.OK, res)
}
