package v1

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/simple_chat/api/handler/v1/http"
	"strconv"
)

func (h *handlerV1) GetMessageList(c *gin.Context) {
	id := c.Query("chat_id")
	roomId, err := strconv.Atoi(id)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}
	res, err := h.Storage.ChatApp().GetMessageList(roomId)
	if err != nil {
		h.handleResponse(c, http.InternalServerError, err)
		return
	}
	h.handleResponse(c, http.OK, res)
}
