package v1

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/simple_chat/api/handler/v1/http"
	"gitlab.com/simple_chat/api/models"
)

func (h *handlerV1) CreateChat(c *gin.Context) {
	var body models.Chat
	err := c.ShouldBindJSON(&body)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err)
		return
	}
	err = h.Storage.ChatApp().CreateChat(&body)
	if err != nil {
		h.handleResponse(c, http.InternalServerError, err)
		return
	}
	h.handleResponse(c, http.Created, models.Message{
		Message: "chat created successfully",
	})
}
