package api

import (
	h "net/http"

	"github.com/casbin/casbin/v2"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	v1 "gitlab.com/simple_chat/api/handler/v1"
	"gitlab.com/simple_chat/api/middleware"
	"gitlab.com/simple_chat/api/tokens"
	"gitlab.com/simple_chat/config"
	"gitlab.com/simple_chat/pkg/logger"
	"gitlab.com/simple_chat/storage"
	"gitlab.com/simple_chat/storage/repo"
)

type Options struct {
	Cfg            config.Config
	Storage        storage.StorageI
	Log            logger.Logger
	CasbinEnforcer *casbin.Enforcer
	Redis          repo.InMemoryStorageI
}

func New(opt *Options) *gin.Engine {
	router := gin.New()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	corConfig := cors.DefaultConfig()
	corConfig.AllowAllOrigins = true
	corConfig.AllowCredentials = true
	corConfig.AllowHeaders = []string{"*"}
	corConfig.AllowBrowserExtensions = true
	corConfig.AllowMethods = []string{"*"}
	router.Use(cors.New(corConfig))

	jwtHandler := tokens.JWTHandler{
		SigninKey: opt.Cfg.SigningKey,
		Log:       opt.Log,
	}
	handlerV1 := v1.New(&v1.HandlerV1Option{
		Cfg:        &opt.Cfg,
		Storage:    opt.Storage,
		Log:        opt.Log,
		JwtHandler: jwtHandler,
		Redis:      opt.Redis,
	})
	router.Use(middleware.NewAuth(opt.CasbinEnforcer, jwtHandler, config.Load()))

	router.GET("/", func(ctx *gin.Context) {
		ctx.JSON(h.StatusOK, gin.H{
			"message": "Server is running!!!",
		})
	})

	router.MaxMultipartMemory = 8 << 20 // 8 Mib

	api := router.Group("/v1")
	// user
	api.GET("/login", handlerV1.Login)
	api.GET("/verify/:code", handlerV1.Verify)
	api.POST("/user", handlerV1.RegisterUser)
	api.GET("/user/:id", handlerV1.GetUser)
	api.GET("/users", handlerV1.GetUserList)
	api.GET("/users/search", handlerV1.GetSearchedUsers)

	api.POST("/chat", handlerV1.CreateChat)
	api.GET("/messages", handlerV1.GetMessageList)

	return router
}
