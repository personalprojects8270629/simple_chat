package websocket

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/simple_chat/storage"
)

func Run(r *gin.Engine, strg storage.StorageI) {

	hub := newHub()
	go hub.run()
	r.GET("/ws", func(c *gin.Context) {
		ServeWs(hub, c, strg)
	})
}
