package storage

import (
	"database/sql"
	"gitlab.com/simple_chat/storage/postgres"
	"gitlab.com/simple_chat/storage/repo"
)

type StorageI interface {
	ChatApp() repo.ChatService
}

type StoragePg struct {
	Db       *sql.DB
	chatrepo repo.ChatService
}

func NewStoragePg(db *sql.DB) *StoragePg {
	return &StoragePg{
		Db:       db,
		chatrepo: postgres.NewStorage(db),
	}
}

func (s StoragePg) ChatApp() repo.ChatService {
	return s.chatrepo
}
