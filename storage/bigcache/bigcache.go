package bigcache

import (
	"github.com/allegro/bigcache/v3"
	"gitlab.com/simple_chat/storage/repo"
	"time"
)

type BigCacheRepo struct {
	Bcach *bigcache.BigCache
}

func NewBigCacheRepo(cache *bigcache.BigCache) repo.InMemoryStorageI {
	return &BigCacheRepo{
		Bcach: cache,
	}
}

func (b *BigCacheRepo) Set(key string, value string) error {
	err := b.Bcach.Set(key, []byte(value))
	if err != nil {
		return err
	}
	return nil
}

func (b *BigCacheRepo) Exists(key string) (interface{}, error) {
	res, err := b.Bcach.Get(key)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (b *BigCacheRepo) SetWithTTL(key, value string, seconds int) error {
	err := b.Bcach.Set(key, []byte(value))
	if err != nil {
		return err
	}
	if seconds != 0 {
		go func() {
			time.Sleep(time.Duration(seconds))
			b.Bcach.Delete(key)
		}()
	}
	return nil
}

func (b *BigCacheRepo) Del(key string) error {
	err := b.Bcach.Delete(key)
	if err != nil {
		return err
	}
	return nil
}

func (b *BigCacheRepo) Get(key string) (interface{}, error) {
	res, err := b.Bcach.Get(key)
	if err != nil {
		return nil, err
	}
	return res, nil
}
