package postgres

import (
	"fmt"
	"gitlab.com/simple_chat/api/models"
	"gitlab.com/simple_chat/pkg/logger"
)

func (r storagePg) CreateUser(req *models.UserRequest) (*models.UserResponse, error) {
	var res = models.UserResponse{}
	query := `
INSERT INTO 
    users(first_name, last_name, user_name, bio, phone, image, email, refresh_token) 
VALUES 
    ($1, $2, $3, $4, $5, $6, $7, $8)
RETURNING
	id, first_name, last_name, user_name, bio, phone, image, email`
	err := r.db.QueryRow(query, req.FirstName, req.LastName, req.UserName, req.Bio, req.Phone, req.Image, req.Email, req.RefreshToken).
		Scan(&res.Id, &res.FirstName, &res.LastName, &res.UserName, &res.Bio, &res.Phone, &res.Image, &res.Email)
	if err != nil {
		logger.Error(err)
		return nil, err
	}
	return &res, nil
}

func (r storagePg) GetUser(id int) (*models.UserResponse, error) {
	res := models.UserResponse{}
	query := `
SELECT 
	id,  first_name, last_name, user_name, bio, phone, image, email
FROM users WHERE id=$1 
`
	err := r.db.QueryRow(query, id).Scan(&res.Id, &res.FirstName, &res.LastName, &res.UserName, &res.Bio, &res.Phone, &res.Image, &res.Email)
	if err != nil {
		logger.Error(err)
		return nil, err
	}
	return &res, nil
}

func (r storagePg) GetUserList() ([]models.Users, error) {
	var res = []models.Users{}
	query := `
SELECT 
	u.id, u.first_name, u.last_name, u.user_name, u.bio, u.image, c.id, c.chat_type
FROM 
    users u 
JOIN 
        user_chat uc1 ON u.id=uc1.user_id 
JOIN 
        user_chat uc2 ON uc1.chat_id=uc2.chat_id
JOIN 
        chats c ON c.id=uc1.chat_id
WHERE 
    uc1.user_id=uc2.user_id
`
	rows, err := r.db.Query(query)
	if err != nil {
		logger.Error(err)
		return nil, err
	}
	for rows.Next() {
		temp := models.Users{}
		err = rows.Scan(&temp.Id, &temp.FirstName, &temp.LastName, &temp.UserName, &temp.Bio, &temp.Image, &temp.ChatId, &temp.ChatType)
		if err != nil {
			logger.Error(err)
			return nil, err
		}
		res = append(res, temp)
	}
	return res, nil
}

func (r storagePg) SearchUsers(key string) ([]models.Users, error) {
	res := []models.Users{}
	query := `
SELECT 
    id, first_name, last_name, user_name, image 
FROM 
    users 
WHERE 
    user_name ILIKE $1 OR first_name ILIKE $2`
	rows, err := r.db.Query(query, "%"+key+"%", "%"+key+"%")
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		temp := models.Users{}
		err = rows.Scan(&temp.Id, &temp.FirstName, &temp.LastName, &temp.UserName, &temp.Image)
		if err != nil {
			return nil, err
		}
		res = append(res, temp)
	}
	return res, nil
}

func (r storagePg) CheckField(req *models.CheckFieldReq) (*models.CheckFieldRes, error) {
	response := models.CheckFieldRes{}
	query := fmt.Sprintf("SELECT 1 FROM users WHERE %s=$1", req.Field)
	err := r.db.QueryRow(query, req.Value).Scan(&response.Exists)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

func (r storagePg) GetUserByEmail(email string) (*models.UserLoginResponse, error) {
	res := models.UserLoginResponse{}
	query := `
SELECT 
    id, 
    first_name, 
    last_name, 
    user_name, 
    bio, 
    email, 
    phone, 
    image 
FROM 
    users WHERE email=$1`
	err := r.db.QueryRow(query, email).
		Scan(
			&res.Id,
			&res.FirstName,
			&res.LastName,
			&res.UserName,
			&res.Bio,
			&res.Email,
			&res.Phone,
			&res.Image)
	if err != nil {
		return nil, err
	}
	return &res, nil
}
