package postgres

import (
	"gitlab.com/simple_chat/api/models"
	"gitlab.com/simple_chat/pkg/logger"
)

func (r storagePg) CreateChat(req *models.Chat) error {
	var chatID int
	err := r.db.QueryRow(`INSERT INTO chats(chat_type) VALUES($1) RETURNING id`, req.ChatType).Scan(&chatID)
	if err != nil {
		logger.Error(err)
		return err
	}

	_, err = r.db.Exec(`
INSERT INTO 
    user_chat(user_id, chat_id) VALUES($1, $2)`, req.UserInfo.User1Id, chatID)
	if err != nil {
		return err
	}

	_, err = r.db.Exec(`
INSERT INTO 
    user_chat(user_id, chat_id) VALUES($1, $2)`, req.UserInfo.User2Id, chatID)
	if err != nil {
		return err
	}
	return nil
}
