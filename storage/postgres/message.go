package postgres

import (
	"gitlab.com/simple_chat/api/models"
	"gitlab.com/simple_chat/pkg/logger"
)

func (r storagePg) CreateMessage(req *models.MessageReq) error {
	query := `
INSERT INTO messages(chat_id, user_id, text) VALUES ($1, $2, $3)`
	_, err := r.db.Exec(query, req.ChatID, req.UserId, req.Text)
	if err != nil {
		logger.Error(err)
		return err
	}
	return nil
}

func (r storagePg) GetMessageList(chatID int) ([]models.Messages, error) {
	res := []models.Messages{}
	query := `
SELECT 
    m.id, m.chat_id, m.user_id, m.text, m.created_at, m.updated_at, u.id, u.first_name, u.last_name, u.image
FROM 
    messages m JOIN users u ON m.user_id=u.id WHERE chat_id=$1`
	rows, err := r.db.Query(query, chatID)
	if err != nil {
		logger.Error(err)
		return nil, err
	}
	for rows.Next() {
		temp := models.Messages{}
		err = rows.Scan(
			&temp.Id, &temp.ChatID,
			&temp.UserId, &temp.Text,
			&temp.CreatedAt, &temp.UpdatedAt,
			&temp.User.Id, &temp.User.FirstName,
			&temp.User.LastName, &temp.User.Image)
		if err != nil {
			logger.Error(err)
			return nil, err
		}
		res = append(res, temp)
	}
	return res, nil
}
