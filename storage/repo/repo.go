package repo

import "gitlab.com/simple_chat/api/models"

type (
	ChatService interface {
		CreateUser(*models.UserRequest) (*models.UserResponse, error)
		GetUser(id int) (*models.UserResponse, error)
		GetUserList() ([]models.Users, error)
		SearchUsers(keyword string) ([]models.Users, error)
		GetUserByEmail(email string) (*models.UserLoginResponse, error)
		//UpdateUser(response *models.UserResponse) error
		//DeleteUser(id int) error
		CheckField(req *models.CheckFieldReq) (*models.CheckFieldRes, error)

		CreateChat(room *models.Chat) error
		CreateMessage(req *models.MessageReq) error
		GetMessageList(roomID int) ([]models.Messages, error)
	}
)

type InMemoryStorageI interface {
	Set(key, value string) error
	SetWithTTL(key, value string, seconds int) error
	Get(key string) (interface{}, error)
	Exists(key string) (interface{}, error)
	Del(key string) (interface{}, error)
	Keys(key string) (interface{}, error)
}
